package com.company;

import com.company.model.FileInfo;
import com.company.model.ObjectDetect;
import com.company.model.Result;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static List<ObjectDetect> objectDetectList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        // Get all object names
        System.out.print("Enter path to file name: ");
        objectDetectList = ObjectDetect.getObjectDetects(input.nextLine());

        // Get all files of test folder
        System.out.print("Enter Test Folder Path:");
        String testRootDirectory = input.nextLine();
        List<FileInfo> testFilesList = FileInfo.getFiles(testRootDirectory);

        // Get all files of result folder
        System.out.print("Enter Result Folder Path:");
        String resultRootDirectory = input.nextLine();
        List<FileInfo> resultFilesList = FileInfo.getFiles(resultRootDirectory);

        // Intersects of Union Ratio
        System.out.print("Enter IOU ratio:");
        double iouRatio = input.nextDouble();

        // Get all files needed
        List<FileInfo> fileNeededList = FileInfo.getFilesNeeded(resultFilesList, testFilesList);

        // Calculate final result
        Result finalResult = Result.calculateResult(fileNeededList, testRootDirectory, resultRootDirectory, iouRatio);

        // Print Summary
        System.out.println("\n------------------Summary------------------");
        System.out.println("IOU ratio: " + iouRatio);
        System.out.println("Total: " + finalResult.getTotal() + " objects");
        System.out.println("TP: " + finalResult.getTruePositive());
        System.out.println("FP: " + finalResult.getFalsePositive());
        System.out.println("FN: " + finalResult.getFalseNegative());
        System.out.println("Correct classification: " + finalResult.getCorrectClassify() + "/" + fileNeededList.size());
        System.out.println("Detection Rate: " + finalResult.getDetectionRates() + "%");
        System.out.println("Classification Rate:  " + finalResult.getClassificationRates() + "%");
    }

   /* public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        // Get all object names
        System.out.print("Enter path to file name: ");
        objectDetectList = ObjectDetect.getObjectDetects(input.nextLine());

        // Get all files of test folder
        System.out.print("Enter Test Folder Path:");
        String testRootDirectory = input.nextLine();
        List<FileInfo> testFilesList = FileInfo.getFiles(testRootDirectory);

        // Get all files of result folder
        System.out.print("Enter Result Folder Path:");
        String resultRootDirectory = input.nextLine();
        List<FileInfo> resultFilesList = FileInfo.getFiles(resultRootDirectory);

        // Get all files needed
        List<FileInfo> fileNeededList = FileInfo.getFilesNeeded(resultFilesList, testFilesList);

        int total = FileInfo.calculationAppearance(objectDetectList, fileNeededList).stream().filter(objectDetect -> objectDetect.getExpectedDetection() >= 0).mapToInt(ObjectDetect::getExpectedDetection).sum();
        System.out.println(total);
    }
     */
}
