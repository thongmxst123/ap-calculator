package com.company.model;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable {
    // Declare Variables
    private int total;
    private int truePositive;
    private int falsePositive;
    private int falseNegative;
    private double detectionRates;
    private int correctClassify;
    private double classificationRates;

    /**
     * Constructor
     */
    public Result() {
    }

    public Result(int total, int truePositive, int falsePositive, int falseNegative, double detectionRates, int correctClassify, double classificationRates) {
        this.total = total;
        this.truePositive = truePositive;
        this.falsePositive = falsePositive;
        this.falseNegative = falseNegative;
        this.detectionRates = detectionRates;
        this.correctClassify = correctClassify;
        this.classificationRates = classificationRates;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTruePositive() {
        return truePositive;
    }

    public void setTruePositive(int truePositive) {
        this.truePositive = truePositive;
    }

    public int getFalsePositive() {
        return falsePositive;
    }

    public void setFalsePositive(int falsePositive) {
        this.falsePositive = falsePositive;
    }

    public int getFalseNegative() {
        return falseNegative;
    }

    public void setFalseNegative(int falseNegative) {
        this.falseNegative = falseNegative;
    }

    public double getDetectionRates() {
        return detectionRates;
    }

    public void setDetectionRates(double detectionRates) {
        this.detectionRates = detectionRates;
    }

    public int getCorrectClassify() {
        return correctClassify;
    }

    public void setCorrectClassify(int correctClassify) {
        this.correctClassify = correctClassify;
    }

    public double getClassificationRates() {
        return classificationRates;
    }

    public void setClassificationRates(double classificationRates) {
        this.classificationRates = classificationRates;
    }

    @Override
    public String toString() {
        return "Result{" +
                "total=" + total +
                ", truePositive=" + truePositive +
                ", falsePositive=" + falsePositive +
                ", falseNegative=" + falseNegative +
                ", detectionRates=" + detectionRates +
                ", correctClassify=" + correctClassify +
                ", classificationRates=" + classificationRates +
                '}';
    }

    /**
     * Summarize the final result
     */
    public static Result calculateResult(List<FileInfo> fileNeededList, String testRootDirectory, String resultRootDirectory, double iouRatio) {
        int fileScanned = 0;
        Result finalResult = new Result();
        for (FileInfo fileInfo : fileNeededList) {
            fileScanned++;
            int[] result = FileInfo.scanningFile(fileNeededList, testRootDirectory, resultRootDirectory, fileScanned, fileInfo, iouRatio);
            finalResult.setTotal(finalResult.getTotal() + result[0]);
            finalResult.setTruePositive(finalResult.getTruePositive() + result[1]);
            finalResult.setFalsePositive(finalResult.getFalsePositive() + result[2]);
            finalResult.setFalseNegative(finalResult.getFalseNegative() + result[3]);
            finalResult.setCorrectClassify(finalResult.getCorrectClassify() + result[4]);
        }
        finalResult.setDetectionRates((double) finalResult.getTruePositive() / finalResult.getTotal() * 100);
        finalResult.setClassificationRates((double) finalResult.getCorrectClassify() / fileNeededList.size() * 100);
        return finalResult;
    }
}
