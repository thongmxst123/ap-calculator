package com.company.model;

import java.io.Serializable;

public class BBox implements Serializable {
    // Define variables
    private double x;
    private double y;
    private double width;
    private double height;

    // Constructor
    public BBox() {

    }

    public BBox(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "BBox{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    /**
     * Calculate area value of bounding box
     */
    public static double bboxArea(BBox bbox) {
        return bbox.getWidth() * bbox.getHeight();
    }

    /**
     * Extract intersected bounding box
     */
    public static BBox intersectBBox(BBox bboxExpected, BBox bboxActual) {
        double left = Math.max(bboxExpected.getX(), bboxActual.getX());
        double top = Math.max(bboxExpected.getY(), bboxActual.getY());
        double right = Math.min(bboxExpected.getX() + bboxExpected.getWidth(), bboxActual.getX() + bboxActual.getWidth());
        double bottom = Math.min(bboxExpected.getY() + bboxExpected.getHeight(), bboxActual.getY() + bboxActual.getHeight());
        double width = right - left;
        double height = bottom - top;
        if (width <= 0 || height <= 0) {
            return null;
        }
        return new BBox(left, top, width, height);
    }

    /**
     * Check if the model correctly detects the object
     */
    public static boolean isDetected(BBox bboxExpected, BBox bboxActual, BBox bboxIntersect, double ratio) {
        double iou = bboxArea(bboxIntersect) / ((bboxArea(bboxExpected) + bboxArea(bboxActual) - bboxArea(bboxIntersect)));
        System.out.println("IOU: " + bboxArea(bboxIntersect) / ((bboxArea(bboxExpected) + bboxArea(bboxActual) - bboxArea(bboxIntersect))) + "");
        return iou >= ratio;
    }
}

