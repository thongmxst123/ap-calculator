package com.company.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ObjectDetect implements Serializable {
    // Define variables
    private int index;
    private String name;
    private int expectedDetection = 0;
    private int actualDetection = 0;

    // Constructor
    public ObjectDetect() {

    }

    public ObjectDetect(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExpectedDetection() {
        return expectedDetection;
    }

    public void setExpectedDetection(int expectedDetection) {
        this.expectedDetection = expectedDetection;
    }

    public int getActualDetection() {
        return actualDetection;
    }

    public void setActualDetection(int actualDetection) {
        this.actualDetection = actualDetection;
    }

    @Override
    public String toString() {
        return "ObjectDetect{" +
                "index=" + index +
                ", name='" + name + '\'' +
                ", expectedDetection=" + expectedDetection +
                ", actualDetection=" + actualDetection +
                '}';
    }

    /**
     * Add objects from file into list
     */
    public static List<ObjectDetect> getObjectDetects(String path) {
        List<ObjectDetect> objectDetectList = new ArrayList<>();
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String objName = myReader.nextLine().trim();
                if (!objName.isEmpty())
                    objectDetectList.add(new ObjectDetect(objectDetectList.size(), objName));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        System.out.println("Total Object:" + objectDetectList.size() + " object");
        return objectDetectList;
    }
}
