package com.company.model;

import java.io.*;
import java.util.*;

public class FileInfo implements Serializable {
    // Define variables
    private String name;
    private String path;

    // Constructor
    public FileInfo() {

    }

    public FileInfo(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    /**
     * Get all txt files in directories
     */
    public static List<FileInfo> getFiles(String path) throws IOException {
        List<FileInfo> fileInfoList = new ArrayList<>();
        File f = new File(path);

        // Filter to get .txt only
        FilenameFilter fileFilter = (dir, name) -> name.toLowerCase().endsWith(".txt");

        File[] files = f.listFiles(fileFilter);
        assert files != null;
        for (File file : files) {
            fileInfoList.add(new FileInfo(file.getName(), file.getCanonicalPath()));
        }
        return fileInfoList;
    }

    /**
     * Get FileInfos needed between two list
     */
    public static List<FileInfo> getFilesNeeded(List<FileInfo> resultFilesList, List<FileInfo> testFilesList) {
        List<FileInfo> actualFilesList = new ArrayList<>();
        for (FileInfo resultFile : resultFilesList) {
            for (FileInfo testFile : testFilesList) {
                if (Objects.equals(resultFile.getName(), testFile.getName())) {
                    actualFilesList.add(resultFile);
                }
            }
        }
        return actualFilesList;
    }

    /**
     * Scanning and compare result
     */
    public static int[] scanningFile(List<FileInfo> fileNeededList, String testRootDirectory, String resultRootDirectory, int fileScanned, FileInfo fileInfo, double iouRatio) {
        List<String> resultStrings = new ArrayList<>();
        List<String> testStrings = new ArrayList<>();
        int[] resultInt = new int[5];
        try {
            System.out.println("\nScanning file " + fileScanned + "/" + fileNeededList.size() + ":");
            File resultFile = new File(resultRootDirectory + "\\" + fileInfo.getName());
            File testFile = new File(testRootDirectory + "\\" + fileInfo.getName());
            Scanner resultReader = new Scanner(resultFile);

            while (resultReader.hasNextLine()) {
                String row = resultReader.nextLine();
                if (!row.isBlank()) {
                    resultStrings.add(row);
                }
            }
            resultReader.close();

            Scanner testReader = new Scanner(testFile);
            while (testReader.hasNextLine()) {
                String row = testReader.nextLine();
                if (!row.isBlank()) {
                    testStrings.add(row);
                }
            }
            testReader.close();

            int total = 0;
            int TP = 0;
            int FP = 0;
            int FN = 0;
            int correctClassify = 0;
            for (String result : resultStrings) {
                List<Double> classifyCorrectList = new ArrayList<>();
                List<Double> confidenceList = new ArrayList<>();
                boolean isTP = false;
                boolean isFP = false;
                char objectType = result.charAt(0);
                Scanner resultScanner = new Scanner(result.substring(2));
                BBox expectedBBox = new BBox(resultScanner.nextFloat(), resultScanner.nextFloat(), resultScanner.nextFloat(), resultScanner.nextFloat());
                for (String test : testStrings) {
                    Scanner testScanner = new Scanner(test.substring(2));
                    BBox actualBBox = new BBox(testScanner.nextFloat(), testScanner.nextFloat(), testScanner.nextFloat(), testScanner.nextFloat());
                    BBox intersectBBox = BBox.intersectBBox(expectedBBox, actualBBox);
                    double confidence = testScanner.nextDouble();
                    confidenceList.add(confidence);
                    if (intersectBBox != null) {
                        if (BBox.isDetected(expectedBBox, actualBBox, intersectBBox, iouRatio)) {
                            isTP = true;
                            if (objectType == test.charAt(0)) {
                                classifyCorrectList.add(confidence);
                            }
                        } else {
                            isFP = true;
                        }
                    }
                }
                try {
                    if (Objects.equals(Collections.max(confidenceList, null), Collections.max(classifyCorrectList, null))) {
                        System.out.println(Collections.max(confidenceList, null));
                        System.out.println(Collections.max(classifyCorrectList, null));
                        correctClassify++;
                    }
                } catch (Exception ignored) {

                }

                if (isTP) {
                    TP++;
                } else if (isFP) {
                    FP++;
                } else {
                    FN++;
                }
                total++;
            }
            resultInt[0] = total;
            resultInt[1] = TP;
            resultInt[2] = FP;
            resultInt[3] = resultStrings.size() > testStrings.size() ? FN + resultStrings.size() - testStrings.size() : FN;
            resultInt[4] = correctClassify;
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return resultInt;
    }

    /**
     * Calculating appearances of single object
     */
    public static List<ObjectDetect> calculationAppearance
    (List<ObjectDetect> objectDetectList, List<FileInfo> fileInfoList) throws FileNotFoundException {
        for (FileInfo file : fileInfoList) {
            File resultFile = new File(file.getPath());
            Scanner fileReader = new Scanner(resultFile);
            while (fileReader.hasNextLine()) {
                String row = fileReader.nextLine();
                if (!row.isBlank()) {
                    int index = Integer.parseInt(String.valueOf(row.charAt(0)));
                    objectDetectList.get(index).setExpectedDetection(objectDetectList.get(index).getExpectedDetection() + 1);
                }
            }
            fileReader.close();
        }
        return objectDetectList;
    }
}
